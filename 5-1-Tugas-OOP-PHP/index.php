<?php

trait Hewan1
{
    public $nama;
    public $jumlahKaki;
    public $darah;
    public $keahlian;

    public function __construct($nama, $jumlahKaki, $keahlian)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }
    public function nama()
    {
        echo "Nama hewan = " . $this->nama . "<br>";
    }
    public function jumlahKaki()
    {
        echo "Jumlah kaki = " . $this->jumlahKaki . "<br>";
    }

    public static function darah()
    {
        echo "Darah = 50 <br>";
    }
    public function keahlian()
    {
        echo "keahlian = " . $this->keahlian . "<br>";
    }

    public function atraksi(): string
    {
        return $this->nama . " sedang " . $this->keahlian;
    }
}

trait Fight1
{
    public $attackPower;
    public $defencePower;

    public function attackPower($attackPower)
    {
        $this->attackPower = $attackPower;
        echo "Attack Power = " . $this->attackPower . "<br>";
    }

    public function defencePower($defencePower)
    {
        $this->defencePower = $defencePower;
        echo "Defence Power = " . $this->defencePower . "<br>";
    }
}

class Elang
{
    use Hewan1, Fight1;
}

class Harimau
{
    use Hewan1, Fight1;
}

$elang = new Elang("Elang", "2", "terbang tinggi");
$elang->nama();
$elang->jumlahKaki();
$elang->darah();
$elang->keahlian();
$elang->attackPower("10");
$elang->defencePower("5");
echo $elang->atraksi() . "<br><br>";

$harimau = new Harimau("Harimau", "4", "lari cepat");
$harimau->nama();
$harimau->jumlahKaki();
$harimau->darah();
$harimau->keahlian();
$harimau->attackPower("8");
$harimau->defencePower("7");
echo $harimau->atraksi() . "<br><br>";
