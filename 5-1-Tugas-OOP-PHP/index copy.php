<?php

trait Hewan
{
    public $nama;
    public $jumlahKaki;
    public $darah;
    public $keahlian;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->jumlahKaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attackPower = $attackPower;
        $this->defencePower = $defencePower;
    }
    public function nama()
    {
        echo "Nama hewan = " . $this->nama . "<br>";
    }
    public function jumlahKaki()
    {
        echo "Jumlah kaki = " . $this->jumlahKaki . "<br>";
    }

    public static function darah()
    {
        echo "Darah = 50 <br>";
    }
    public function keahlian()
    {
        echo "keahlian = " . $this->keahlian . "<br>";
    }

    public function atraksi(): string
    {
        return $this->nama . " sedang " . $this->keahlian;
    }
}

trait Fight
{
    use Hewan;

    public $attackPower;
    public $defencePower;
    public $diserang;

    public function attackPower()
    {
        echo "Attack Power = " . $this->attackPower . "<br>";
    }

    public function defencePower()
    {
        echo "Defence Power = " . $this->defencePower . "<br>";
    }

    public function serang($diserang)
    {
        $this->diserang = $diserang;
        echo $this->nama . " sedang menyerang " . $this->diserang . "<br><br>";
    }

    public function diserang()
    {
        echo $this->nama . "sedang di serang";
        $this->darahsekarang = $darah - $attackPower / $defencePower;
        echo "Darahmu berkurang menjadi" . $this->darahsekarang;
    }
}

class Elang
{
    use Fight;
}

class Harimau
{
    use Fight;
}

$elang = new Elang("Elang", "2", "terbang tinggi", "10", "5");
$elang->nama();
$elang->jumlahKaki();
$elang->darah();
$elang->keahlian();
$elang->attackPower();
$elang->defencePower();
echo $elang->atraksi() . "<br>";
$elang->serang("Harimau");

$harimau = new Harimau("Harimau", "4", "lari cepat", "8", "7");
$harimau->nama();
$harimau->jumlahKaki();
$harimau->darah();
$harimau->keahlian();
$harimau->attackPower();
$harimau->defencePower();
echo $harimau->atraksi() . "<br><br>";
